from django.test import TestCase, LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from time import sleep

class IndexUnitTest(TestCase):

    def test_get_success(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

# class IndexFunctionalTest(LiveServerTestCase):

#     def setUp(self):
#         options = Options()
#         options.add_argument('--dns-prefetch-disable')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--headless')
#         options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', options=options)
#         super().setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super().tearDown()

#     def test_add_favourite(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         favourite = selenium.find_element_by_id('favourite_count')
#         self.assertEqual(favourite.text, '0')
#         sleep(3)
#         button = selenium.find_element_by_class_name('favourite')
#         button.click()
#         self.assertEqual(favourite.text, '1')

class RestUnitTest(TestCase):

    def test_get_success(self):
        response = self.client.get('/endpoint/')
        self.assertEqual(response.status_code, 200)

    def test_get_optional_param(self):
        response = self.client.get('/endpoint/animal/')
        self.assertEqual(response.status_code, 200)

class LoginUnitTest(TestCase):

    def test_login_success(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_success_redirected(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)

class StarUnitTest(TestCase):

    def test_post_success(self):
        # get id of book according to search result
        response = self.client.get('/endpoint/')
        data = response.json()
        id = data['items'][0]['id']
        self.client.post('/star/', {'id': id, 'favorited': 'true'})
        response = self.client.get('/endpoint/')
        data = response.json()
        self.assertEqual(data['countStar'], 1)

    def test_post_fail(self):
        response = self.client.post('/star/', {'id': 'ckKQWGpJ_1kC'})
        self.assertEqual(response.status_code, 400)
