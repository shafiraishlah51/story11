window.keyword = "quilting"

$(document).ready(function() {
    getData()
    inputClickOnEnter()
})

function getData(keyword = window.keyword) {
    $.get('/endpoint/' + keyword, function(result) {
        resetHtml(keyword)
        result.items.forEach(function(book, index) {
            $('#content').append(`
                <tr>
                    <th scope="row">${index+1}</th>
                    <td>${book.volumeInfo.title}</td>
                    <td>${book.volumeInfo.authors ?
                        book.volumeInfo.authors.join(', ') :
                        '-'}</td>
                    <td>${book.volumeInfo.publisher}</td>
                    <td class="text-center">
                        <img class="star favourite"
                            src="/https://i.imgur.com/7CeVmZu.png"
                            id='${book.id}'/>
                    </td>
                </tr>
            `)
            if (book.favorited) {
                displayStarNotClicked.bind($('#' + book.id)[0])()
            } else {
                displayStarClicked.bind($('#' + book.id)[0])()
            }
        })
        $('#favourite_count').html(result.countStar)
    })
}

function inputClickOnEnter() {
    $("#kategori").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#button-kategori").click();
        }
    })
}

function resetHtml(keyword) {
    $('#content').html('')
    $('#favourite_count').html('0')
}

function lightStar() {
    $(this).attr('src', 'https://i.imgur.com/SPTSn8Y.png')
}

function darkStar() {
    $(this).attr('src', 'https://i.imgur.com/7CeVmZu.png')
}

function displayStarNotClicked() {
    var nextCnt = parseInt($("#favourite_count").html()) + 1
    $("#favourite_count").html(nextCnt.toString())
    lightStar.bind(this)()
    $(this).off()
    $(this).mouseover(darkStar)
    $(this).mouseout(lightStar)
    $(this).click(starClickedFav)
}

function starClickedNotFav() {
    displayStarNotClicked.bind(this)()
    $.post('/star/', {id: $(this).attr('id'), favorited: true})
}

function displayStarClicked() {
    var nextCnt = parseInt($("#favourite_count").html()) - 1
    $("#favourite_count").html(nextCnt.toString())
    darkStar.bind(this)()
    $(this).off()
    $(this).mouseover(lightStar)
    $(this).mouseout(darkStar)
    $(this).click(starClickedNotFav)
}

function starClickedFav() {
    displayStarClicked.bind(this)()
    $.post('/star/', {id: $(this).attr('id'), favorited: false})
}

function searchCategory(element) {
    window.keyword = $("#kategori").val()
    getData(keyword)
}
